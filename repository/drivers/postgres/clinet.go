package postgres

import (
	"os"
	"sync"

	"bitbucket.org/evhivetech/relocation-service/app"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	log "github.com/sirupsen/logrus"
)

type PostgresClientHandler interface {
	Connect() PostgresClientHandler
	DBPing() error
	GetPostgresDbString() string
	GetSession() *gorm.DB
}

type PostgresClient struct {
	db *gorm.DB
}

var once sync.Once
var conn *PostgresClient

func New() PostgresClientHandler {
	//singleton pattern
	once.Do(func() {
		conn = &PostgresClient{}
	})
	return conn
}

func (r *PostgresClient) Connect() PostgresClientHandler {
	var err error
	r.db, err = gorm.Open("postgres", r.GetPostgresDbString())
	if err != nil {
		log.Error("failed to connect database")
	}

	if err = r.db.DB().Ping(); err != nil {
		log.Error("failed to connect database")
	}

	return r
}

func (r *PostgresClient) DBPing() error {
	if err := r.db.DB().Ping(); err != nil {
		log.Error("failed to connect database")
		return err
	}
	return nil
}

func (r *PostgresClient) GetPostgresDbString() string {
	var connectionString string
	if os.Getenv("DATABASE_CONNECTION_STRING") == "" {
		connectionString = "postgresql://" + app.AppConfig.Database.Username + ":" + app.AppConfig.Database.Password + "@" + app.AppConfig.Database.Host + "/" + app.AppConfig.Database.DB + "?" + app.AppConfig.Database.SSLMode

	} else {
		connectionString = os.Getenv("DATABASE_CONNECTION_STRING")
	}

	return connectionString
}

func (r *PostgresClient) GetSession() *gorm.DB {
	return r.db
}
