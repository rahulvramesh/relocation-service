package main

import (
	"net/http"

	r "bitbucket.org/evhivetech/relocation-service/app/http/routes"
	"bitbucket.org/evhivetech/relocation-service/repository/drivers/postgres"
	"bitbucket.org/evhivetech/relocation-service/usecase/cases"

	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

func main() {

	//connect to db
	connectDB := postgres.New().Connect().GetSession()
	defer connectDB.Close()

	//initialize usecase
	relocationRepo := postgres.NewPostgresRelocationRepository(connectDB)

	//initialize usecase
	relocationUsecase := cases.NewRelocationUsecase(relocationRepo)

	//initialize router
	router := mux.NewRouter().StrictSlash(false)

	//relocation router
	router = r.NewRelocationRouter(
		router,
		relocationUsecase,
	)

	//rest of settings
	n := negroni.Classic()
	//n.Use(negroni.HandlerFunc(middleware.FetchDevConUser))
	//set panic recovery
	recovery := negroni.NewRecovery()
	recovery.Formatter = &negroni.HTMLPanicFormatter{}
	n.Use(recovery)
	n.UseHandler(router)

	//Listen Config
	server := &http.Server{
		Addr: "0.0.0.0:8005",
		// ReadTimeout:  time.Duration(svc.ReadTimeout) * time.Second,
		// WriteTimeout: time.Duration(svc.WriteTimeout) * time.Second,
		Handler: n,
	}

	log.Info("Relocation Service Running At :8005")
	server.ListenAndServe()

}
