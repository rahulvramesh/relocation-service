package routes

import (
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"

	handlers "bitbucket.org/evhivetech/relocation-service/app/http/controllers"
	relocationUsecase "bitbucket.org/evhivetech/relocation-service/usecase"
)

func NewRelocationRouter(
	router *mux.Router,
	us relocationUsecase.RelocationUsecase,
) *mux.Router {

	handler := &handlers.HttpRelocationHandler{
		RelocationUsecase: us,
	}

	relocationRouter := mux.NewRouter()
	relocationRouter.HandleFunc("/api/v1/relocation/relocate", handler.Add).Methods("POST")
	router.PathPrefix("/api/v1/relocation").Handler(negroni.New(
		negroni.Wrap(relocationRouter),
	))

	return router
}
