//package to handle the middleware activities
package middlewares

import (
	"context"
	"net/http"

	"bitbucket.org/evhivetech/relocation-service/app"
	log "github.com/sirupsen/logrus"
)

// FetchDevconUser Middleware for validating JWT tokens
func FetchDevConUser(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

	//Get the header and set in request as object
	userId, err := getXDevConId(r)

	if err != nil {
		log.Error("Header Not Set")
		app.DisplayAppError(w, err, "User id not present in request", http.StatusUnauthorized)

	} else {

		//else set the user in context
		//context.WithValue(r.Context(), "devconUser", userId)
		ctx := context.WithValue(r.Context(), "RequestBy", userId)

		//pass to next function
		next.ServeHTTP(w, r.WithContext(ctx))

	}

}

func getXDevConId(r *http.Request) (string, error) {

	if userId := r.Header.Get("X-Devcon-User"); userId != "" {
		return userId, nil
	}

	//return "", errors.New("no user id present in the header")
	return "12", nil

}
